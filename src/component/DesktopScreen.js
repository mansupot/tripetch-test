import styled from 'styled-components';
import athletes1 from '../assets/athletes1.png';
import athletesTablet1 from '../assets/athletes-tablet1.png';
import athletes2 from '../assets/athletes2.png';

const Container = styled.div`
  overflow: hidden;
  background-color: white;
  height: auto;
  margin: 0;
  padding: 0;

  //media query less than 768px
  @media (max-width: 767px) {
    display: none;
  }
`;
const Card = styled.section`
  height: 830px;
  width: 100%;
`;
const TitleSection = styled.section`
  background-color: #ffffff;
  min-height: 115px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  margin-top: 100px;
`;
const TopSection = styled.section`
  background-color: #ffffff;
  min-height: 210px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
`;
const MiddleSection = styled.section`
  background-color: #f5f4f9;
  min-height: 266px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
`;
const BottomSection = styled.section`
  background-color: ${props => props.bgColor || '#f5f4f9'};
  min-height: 238px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
`;
const Wrapper = styled.div`
  width: ${props => props.width || '50%'};

  @media (min-width: 1920px) {
    ${props => props.left && 'padding-left: 322px;'}
    ${props => props.right && 'padding-left: 48px; position: relative;'}
  }

  @media (min-width: 768px) and (max-width: 1919px) {
    width: ${props => (props.left ? '40%' : '60%')};
    ${props => props.left && props.keepLeft && 'width: 60%; padding-left: 30px;'}
    ${props => props.right && 'width: 60%; padding-left: 0px;'}
    ${props => props.right && props.keepRight && 'width: 40%; padding-left: 0px; position: relative;'}
  }
`;
const TitleText = styled.div`
  color: #e7e7e7;
  font-family: Roboto;
  font-size: 90px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  margin-bottom: 10px;
`;
const SubTitle = styled.div`
  color: #c2c2c2;
  font-family: Roboto;
  font-size: 36px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  letter-spacing: 1.5px;
  margin-top: 60px;
  flex-direction: row;
  display: flex;
`;
const Description = styled.div`
  color: ${props => props.colorText || '#000000'};
  font-family: Roboto;
  font-weight: 400;
  line-height: 28px;

  @media (min-width: 1920px) {
    font-size: 20px;
    margin-top: 20px;
    margin-right: ${props => (props.keepLeft ? '' : '195px')};
  }

  @media (min-width: 768px) and (max-width: 1919px) {
    font-size: 18px;
    margin-top: 20px;
    margin-right: ${props => (props.keepLeft ? '' : '30px')};
  }
`;
const No = styled.span`
  color: ${props => props.color || '#000000'};
  font-family: Roboto;
  font-size: 18px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  letter-spacing: 1.5px;
  margin-right: 10px;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;

  &::after {
    content: '';
    display: flex;
    width: 22px;
    height: 5px;
    background-color: ${props => (props.dark ? '#FFFFFF' : '#5e3db3')};
    border-radius: 2.5px;
    margin-top: 4px;
  }
`;
const WrapperImage = styled.div`
  position: absolute;
  overflow: hidden;

  @media (min-width: 1920px) {
    ${props => props.left && 'left: 25px; transform: translateX(50%);'}
    ${props => props.right && 'transform: translate(-20%, -10%);'}
  }

  @media (min-width: 768px) and (max-width: 1400px) {
    ${props => props.left && 'left: 25px; transform: translateX(-35%);'}
    ${props => props.right && 'transform: translate(-17%, -10%);'}
  }
`;

const Athletes1 = styled.div`
  background-repeat: no-repeat;
  background-size: cover;

  @media (min-width: 1920px) {
    width: 678px;
    height: 950px;
    background-image: url(${athletes1});
  }
  @media (min-width: 768px) and (max-width: 1919px) {
    margin-top: 30px;
    width: 518px;
    height: 699px;
    background-image: url(${athletesTablet1});
  }
`;
const Athletes2 = styled.div`
  background-repeat: no-repeat;
  background-size: cover;

  background-image: url(${athletes2});
  @media (min-width: 1920px) {
    width: 991px;
    height: 815px;
  }
  @media (min-width: 768px) and (max-width: 1919px) {
    margin-top: 30px;
    width: 691px;
    height: 568px;
  }
`;

function DesktopScreen() {
  return (
    <Container>
      <Card>
        <TitleSection>
          <Wrapper left>
            <WrapperImage left>
              <Athletes1 />
            </WrapperImage>
          </Wrapper>
          <Wrapper right>
            <TitleText>ATHLETES</TitleText>
          </Wrapper>
        </TitleSection>
        <TopSection>
          <Wrapper left />
          <Wrapper right>
            <SubTitle>
              <No>01</No>CONNECTION
            </SubTitle>
            <Description>Connect with coaches directly, you can ping coaches to view profile.</Description>
          </Wrapper>
        </TopSection>
        <MiddleSection>
          <Wrapper left />
          <Wrapper right>
            <SubTitle>
              <No>02</No>COLLABORATION
            </SubTitle>
            <Description>
              Work with other student athletes to  increase visability. When you share and like other players videos it will increase your visability as a player. This is the team
              work aspect to Surface 1.
            </Description>
          </Wrapper>
        </MiddleSection>
        <BottomSection bgColor="#5E3DB3">
          <Wrapper left />
          <Wrapper right>
            <SubTitle>
              <No dark>03</No>GROWTH
            </SubTitle>
            <Description colorText="#ffffff">Resources and tools for you to get better as a student Athelte. Access to training classes, tutor sessions, etc </Description>
          </Wrapper>
        </BottomSection>
      </Card>
      <Card>
        <TitleSection>
          <Wrapper left keepLeft>
            <TitleText>PLAYERS</TitleText>
          </Wrapper>
          <Wrapper right keepRight>
            <WrapperImage right>
              <Athletes2 />
            </WrapperImage>
          </Wrapper>
        </TitleSection>
        <TopSection>
          <Wrapper left keepLeft>
            <SubTitle>
              <No>01</No>CONNECTION
            </SubTitle>
            <Description keepLeft>Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.</Description>
          </Wrapper>
          <Wrapper right keepRight />
        </TopSection>
        <MiddleSection>
          <Wrapper left keepLeft>
            <SubTitle>
              <No>02</No>COLLABORATION
            </SubTitle>
            <Description keepLeft>Work with recruiter to increase your chances of finding talented athlete.</Description>
          </Wrapper>
          <Wrapper right keepRight />
        </MiddleSection>
        <BottomSection bgColor="#090C35">
          <Wrapper left keepLeft>
            <SubTitle>
              <No dark color="#8F6BE8">
                03
              </No>
              GROWTH
            </SubTitle>
            <Description colorText="#ffffff" keepLeft>
              Save your time, recruit proper athlets for your team.
            </Description>
          </Wrapper>
          <Wrapper right keepRight />
        </BottomSection>
      </Card>
    </Container>
  );
}

export default DesktopScreen;
