import React from 'react';
import styled from 'styled-components';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';

import athletesMobile1 from '../assets/athletes-mobile1.png';
import athletes2 from '../assets/athletes2.png';

const Container = styled.div`
  @media (max-width: 767px) {
    overflow-x: hidden;
    background-color: white;
    height: auto;
    margin: 0;
    padding: 0;
  }
  @media (min-width: 768px) {
    display: none;
  }
`;
const Card = styled.section`
  @media (max-width: 767px) {
    min-height: 568px;
    width: 100%;
  }
`;
const CardTitle = styled.div`
  background-color: #ffffff;
  /* min-height: 115px; */
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  margin-top: 20px;
  margin-left: 20px;
`;

const TitleText = styled.div`
  color: #e7e7e7;
  font-family: Roboto;
  font-size: 50px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
`;
const AthletesTabletImage1 = styled.img.attrs({
  src: athletesMobile1,
  alt: 'athletes1',
})`
  width: 218px;
  height: 281px;
`;
const AthletesTabletImage2 = styled.img.attrs({
  src: athletes2,
  alt: 'athletes2',
})`
  width: 302px;
  height: 250px;
`;
const CardImage = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  width: 100%;
  height: ${props => props.height || '220px'};
  margin-top: 15px;
`;
const CardContent = styled.div`
  height: 258px;
  background-color: #f5f4f9;
`;
const SubTitle = styled.div`
  color: #c2c2c2;
  font-family: Roboto;
  font-size: 28px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  letter-spacing: 1.5px;
  margin-top: 60px;
  flex-direction: row;
  display: flex;
`;
const No = styled.span`
  color: #000000;
  font-family: Roboto;
  font-size: 15px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  margin-right: 10px;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;

  &::after {
    content: '';
    display: flex;
    width: 22px;
    height: 5px;
    background-color: #5e3db3;
    border-radius: 2.5px;
    margin-top: 4px;
  }
`;
const WrapperContent = styled.div`
  padding: 20px 20px 5px 20px;
`;
const Description = styled.div`
  color: #000;
  font-family: Roboto;
  font-size: 15px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;

  margin: 10px 0 30px 00px;
`;

function MobileScreen() {
  return (
    <Container>
      <Card>
        <CardTitle>
          <TitleText>ATHLETES</TitleText>
        </CardTitle>
        <CardImage>
          <AthletesTabletImage1 />
        </CardImage>
        <CardContent>
          <Swiper modules={[Pagination]} spaceBetween={50} slidesPerView={1} pagination={{ clickable: true }}>
            <SwiperSlide>
              <WrapperContent>
                <SubTitle>
                  <No>01</No>CONNECTION
                </SubTitle>
                <Description>Connect with coaches directly, you can ping coaches to view profile.</Description>
              </WrapperContent>
            </SwiperSlide>
            <SwiperSlide>
              <WrapperContent>
                <SubTitle>
                  <No>02</No>COLLABORATION
                </SubTitle>
                <Description>
                  Work with other student athletes to  increase visability. When you share and like other players videos it will increase your visability as a player. This is the
                  team work aspect to Surface 1.
                </Description>
              </WrapperContent>
            </SwiperSlide>
            <SwiperSlide>
              <WrapperContent>
                <SubTitle>
                  <No>03</No>
                  GROWTH
                </SubTitle>
                <Description>Save your time, recruit proper athlets for your team.</Description>
              </WrapperContent>
            </SwiperSlide>
          </Swiper>
        </CardContent>
      </Card>
      <Card>
        <CardTitle>
          <TitleText>PLAYERS</TitleText>
        </CardTitle>
        <CardImage height="220px">
          <AthletesTabletImage2 />
        </CardImage>
        <CardContent>
          <Swiper modules={[Pagination]} spaceBetween={50} slidesPerView={1} pagination={{ clickable: true }}>
            <SwiperSlide>
              <WrapperContent>
                <SubTitle>
                  <No>01</No>CONNECTION
                </SubTitle>
                <Description>Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.</Description>
              </WrapperContent>
            </SwiperSlide>
            <SwiperSlide>
              <WrapperContent>
                <SubTitle>
                  <No>02</No>COLLABORATION
                </SubTitle>
                <Description>Work with recruiter to increase your chances of finding talented athlete.</Description>
              </WrapperContent>
            </SwiperSlide>
            <SwiperSlide>
              <WrapperContent>
                <SubTitle>
                  <No>03</No>
                  GROWTH
                </SubTitle>
                <Description>Save your time, recruit proper athlets for your team.</Description>
              </WrapperContent>
            </SwiperSlide>
          </Swiper>
        </CardContent>
      </Card>
    </Container>
  );
}

export default MobileScreen;
