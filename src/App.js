import React from 'react';
import DesktopScreen from './component/DesktopScreen';
import MobileScreen from './component/MobileScreen';

import 'swiper/css/bundle';

function App() {
  return (
    <React.Fragment>
      <DesktopScreen />
      <MobileScreen />
    </React.Fragment>
  );
}

export default App;
